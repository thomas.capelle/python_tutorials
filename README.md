# Python Tutorials
> A set of notebooks with various examples

## Notebooks

- [01_python](01_python.ipynb): Basic Python, tips and tricks
    - Iterables, Generators
    - pathlib
- [02_pandas](02_pandas.ipynb): Some pandas magic
    - csv reading
    - timeseries immporting/formatting
    - plotting
    - MultiIndex